const express = require ('express');
const { verifyAdmin } = require('../auth');
const router = express.Router();
const auth = require('../auth');
const {verify} = auth;
// const {verifyAdmin} = auth;


//! CONTROLLERS SECTION
const flightControllers = require('../controllers/flightControllers');

//! ADMIN ADD FLIGHTS
router.post('/add-flight',verify, verifyAdmin ,flightControllers.addFlight);

// !GET FLIGHTS (ACTIVE ONLY FOR USERS)
router.get ('/getFlights',verify, flightControllers.getFlights);

//! ADMIN GET ALL FLIGHTS
router.get('/flights', verify,verifyAdmin, flightControllers.getAllFlights);

// ! DEACTIVATE FLIGHTS [ADMIN]
router.put('/deactFlights/:id', verify, verifyAdmin, flightControllers.deactivateFlights);

// ! GET INACTIVE FLIGHTS ADMIN
router.get('/inactive-flights', verify,verifyAdmin, flightControllers.getInactiveFlights);

// ! SEARCH!
router.post('/search', flightControllers.search);

module.exports = router;