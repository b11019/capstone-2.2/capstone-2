const express = require ('express');
// const { verify } = require('jsonwebtoken');
const  auth  = require('../auth');
const router = express.Router();

const {verify, verifyAdmin} = auth;


const userControllers = require('../controllers/userControllers');
// ! GROUP ROUTING [SECTION]

//! REGISTER USER
router.post('/register', userControllers.register);

// ! LOGIN FOR USERS
router.post ('/login', userControllers.login);


// ! ADMIN ONLY (GET ALL USERS)
router.get('/admin-get-user', verify, verifyAdmin, userControllers.getAllUser);

// ! NON -ADMIN (DEACTIVATE ACCOUNT)
router.put('/deactivate-account/:id',verify, userControllers.deactivateUser);

// ! ADMIN (GET INACTIVE / DEACTIVATED ACCOUNT)
router.get('/deactivated-accounts', verify, verifyAdmin, userControllers.getInactiveUser);

// ! ADMIN DELETE DEACTIVATED ACCOUNT
router.get('/delete-user', verify, verifyAdmin, userControllers.deleteInactiveUser);

// ! SET A USER TO ADMIN (ADMIN)
router.put('/activate-admin/:id', verify, verifyAdmin, userControllers.makeAdmin);


//! BOOK A FLIGHT (USER)
router.post('/booking', verify, userControllers.bookings);

// ! GET BOOKED FLIGHTS 

router.get('/get-booking', verify, userControllers.getActiveBooking);

// ! DELETE BOOKED FLIGHTS

// router.post('/delete-booked', verify, userControllers.deleteBooking);

// ! 
router.get('/bookings',verify, verifyAdmin, userControllers.viewAllFlights);

// ! Check email and user exists
router.post('/check', userControllers.checkEmailExist);



module.exports = router;