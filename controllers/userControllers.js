const User = require ('../models/User');
const Flight = require('../models/Flight')
const bcrypt = require('bcryptjs');
const auth = require('../auth');


// ! REGISTER USER
module.exports.register = (req,res) => {
    console.log(req.body);
    const securedPW = bcrypt.hashSync(req.body.password, 10);
    let newUser = new User ({
        fullName : req.body.fullName,
        email : req.body.email,
        password : securedPW
    }) 
    newUser.save()
    .then(user => res.send (user))
    .catch(err => res.send (err));
};

// ! LOGIN USER
module.exports.login = (req, res) => {
    console.log(req.body);
    User.findOne({email : req.body.email})
    .then(foundUser => {
        if(foundUser === null) { 
            res.send('User not found! Please check your credentials')
        
        }else {
            const correctPassword = bcrypt.compareSync(req.body.password, foundUser.password)
            console.log(correctPassword);
            
            if(correctPassword){
                
                return res.send({accessToken: auth.createAccessToken(foundUser)})
            } else {
                return res.send("Incorrect credentials, please try again")
            }
        }
    })
}



// ! ADMIN TO GET ALL USERS
module.exports.getAllUser =  (req,res) => {
      User.find({}) 
      .then(result => res.send(result))
      .catch(err => res.send(err))
    
};

// ! USER OPTION TO DEACTIVATE ITS ACCOUNT
module.exports.deactivateUser = (req, res) => {
    console.log(req.body)
    let denyUser = {
        status : false
    }
    User.findByIdAndUpdate(req.params.id, denyUser, {new:true})
    .then(deactivateUser => res.send(deactivateUser))
    .catch(err => res.send(err));
}

// ! ADMIN [TO GET INACTIVE ACCOUNTS]
module.exports.getInactiveUser = (req,res) => {
    console.log(req.body);
    User.find({status : false})
    .then(result => {
        if(result !== null  ){
            return res.send(result)
        } else 
            return res.send(err)
    })
    .catch(err => res.send(err))
}

// ! DELETE INACTIVE ACCOUNT
module.exports.deleteInactiveUser = (req,res) => {
   User.findOneAndDelete({status : false})
    .then(inactiveUser => res.send(inactiveUser))
    .catch(err => res.send(err));
};

//! FOR ADMIN - MAKING USER AN ADMIN

module.exports.makeAdmin = (req,res) => {
    let newAdmin = {
        isAdmin : true
    }
    User.findByIdAndUpdate(req.params.id, newAdmin, {new: true})
    .then(newAdmin => res.send(newAdmin))
    .catch(err => res.send(err))
}
// ! USER TO BOOK A FLIGHT
module.exports.bookings = async (req,res) => {
    console.log(req.user.id)
    console.log(req.body.flightId)

    if(req.user.isAdmin){
        return res.send("ACTION FORBIDDEN")
    }
    let isUserUpdated = await User.findById(req.user.id)
    .then(user => {
        console.log(user)

        let newBooking = {
            flightId : req.body.flightId,
            price : req.body.price
        }
        user.booking.push(newBooking)
        return user.save()
        .then(user => true)
        .catch(err => (err.message))
    })
    if(isUserUpdated !== true){
        return res.send({message: isUserUpdated})
    }
    let isFlightUpdated = await Flight.findById(req.body.flightId)
    .then(Flight =>{
        console.log(Flight)
        let bookedUser = {
            userId : req.user.id
        }
        Flight.bookedUser.push(bookedUser)
        return Flight.save()
        .then(Flight => true)
        .catch(err => err.message)
    })
    if (isFlightUpdated !== true){
        return res.send({message: isFlightUpdated})
    }
    if (isUserUpdated && isFlightUpdated){
        return res.send({message: "Booked Successfully"})
    }
}



// ! GET TOTAL AMOUNT TO PAY
// module.exports.totalPayable =  (req, res) => {
//     let sum = 0;
//     let priceArray = {req.user.booking}

//     for (let i = 0; i < Array.length, i++){
//         sum += priceArray[req.user.booking]
//     }
// }

module.exports.getActiveBooking = (req, res) => {
    
    User.find({isAdmin : false && booking !== 0  })
    .then(result => {
        if(result !== null  ){
            return res.send(result)
        } else 
            return res.send(err)
    })
    .catch(err => res.send(err));
};

// module.exports.deleteBooking = async (req,res) => {
    
//     console.log(req.user.id);
//     console.log(req.body.flightId);

//     if (req.user.isAdmin){
//         return res.send("ACTION FORBIDDEN")
//     }

//     let isUserUpdated = await User.findByIdAndRemove(req.user.id).then(user => {
//         console.log(user)


//         let newBooking = {
//             flightId : req.body.flightId,
            
//         }
//         user.booking.push(newBooking)
//         return user.save()
//         .then(user => true)
//         .catch(err => err.message)
//     })
//         if (isUserUpdated !== true){
//             return res.send({message : isUserUpdated})
//         }

//         let isFlightUpdated = await Flight.findById(req.body.flightId)
//         .then(Flight => {
//             console.log(Flight)
//             let bookedUser = {
//                 userId : req.user.id
//             }
//             Flight.bookedUser.push(bookedUser)
//             return Flight.save()
//             .then(Flight => true)
//             .catch(err => err.message)
//         })
//         if (isFlightUpdated !== true){
//             return res.send({message : isFlightUpdated})
//         }
//         if (isUserUpdated && isFlightUpdated){
//             return res.send({message:"Booked successfully"})
//         }
// }



module.exports.viewAllFlights = (req, res) => {
    let arr = [];
    Flight.find({})
        .then((result) => {
            result.forEach((flight) => {
                console.log(flight);
                flight.bookedUser.length !== 0 &&
                    arr.push({
                       destination : flight.destination,
                       price : flight.price,
                       bookedDate : flight.bookedDate  
                    });
            });
            
            console.log(arr);

            res.send({ arr });
        })
        .catch((err) => res.send(err));
};

module.exports.checkEmailExist = (req,res) => {
    console.log(req.body);
    User.findOne({email: req.body.email}).then(result => {

        if(result !== null && result.email === req.body.email){
            return res.send('Email already exists')
        } else if (result === null) {

            return res.send ('Email is available')
            
        }
    })
    .catch(error => res.send(error))

}