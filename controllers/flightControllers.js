const Flight = require('../models/Flight');

//! ADMIN [ADD FLIGHT]
module.exports.addFlight = (req,res) => {
    console.log(req.body)
    let newFlight = new Flight ({
        destination : req.body.destination,
        price : req.body.price,
        date : req.body.date
    })
    newFlight.save()
    .then(flight => res.send(flight))
    .catch(err => res.send(err));
}

// ! USER GET ALL ACTIVE FLIGHTS
module.exports.getFlights = (req,res) => {
    console.log(req.body);
    Flight.find({status : true})
    .then(result => {
        if(result !== null  ){
            return res.send(result)
        } else 
            return res.send(err)
    })
    .catch(err => res.send(err))
}

// ! GET ALL FLIGHTS ACTIVE OR NOT - ADMIN ONLY
module.exports.getAllFlights = (req,res) => {
    
    Flight.find({})
    .then(result =>  res.send(result))
    .catch(err => res.send(err));
}

// ! DEACTIVATE FLIGHTS
module.exports.deactivateFlights = (req,res) => {
    let updates = {
        status : false
    }
    Flight.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(deactivateFlights => res.send(deactivateFlights))
    .catch(err => res.send(err));
    
};
//! GET INACTIVE FLIGHTS - ADMIN
module.exports.getInactiveFlights = (req,res) => {
    Flight.find({status : false})
    .then(result => {
        if(result !== null  ){
            return res.send(result)
        } else 
            return res.send(err)
    })
    .catch(err => res.send(err))
}

// ! FIND FLIGHT 

module.exports.search = (req, res) => {
    console.log(req.body)
    Flight.find({destination : {$regex : req.body.destination ,$options : '$i'}})
    .then(result => {
        if(result.length === 0){
            return res.send('Unable find flight')
        } else {
            return res.send(result)
        }
    })
}

// ! 