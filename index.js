const express = require('express');
const mongoose = require ('mongoose');
const cors = require('cors');
const { urlencoded } = require('express');


const app = express();
const PORT = process.env.PORT ||4000;
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.hpwzu.mongodb.net/capstone-2?retryWrites=true&w=majority",
{
    useNewUrlParser : true,
    useUnifiedTopology: true
}
);

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Failed to connect to database'));
db.once('open', ()=> console.log('Successfully connected to database'))

app.use(express.json());
app.use(urlencoded());
app.use(cors());


const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

const flightRoutes = require ('./routes/flightRoutes');
app.use('/flights',flightRoutes);


app.listen(PORT, () => console.log(`Server is now running at port ${PORT}`));

