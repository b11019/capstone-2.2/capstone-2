const mongoose = require('mongoose');
const Flight = require('../models/Flight')
const userSchema = mongoose.Schema ({
    fullName : {
        type : String,
        required : true
    },
    email : {
        type : String,
        required : true
    },
    password : {
        type : String,
        required : true
    },
    createdOn : {
        type : Date,
        required : true,
        default : Date.now()
    },
    status : {
        type: Boolean,
        default : true
    },
    isAdmin : {
        type : Boolean,
       
        default : false
    },
    booking : [
        {
            flightId : {
                type : String,
                required : true
            },
            status : {
                type : Boolean,
                default : true
            },
            price : {
                type : Number,
                required : true
            },
            bookedDate: {
                type: Date,
                default: new Date()
            }

        }
    ]
})

module.exports = mongoose.model("User", userSchema);